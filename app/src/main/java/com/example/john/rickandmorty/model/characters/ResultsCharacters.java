package com.example.john.rickandmorty.model.characters;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ResultsCharacters {

    public ResultsCharacters() {

    }

    @SerializedName("results")
    public List<Character> characterResults;
}
