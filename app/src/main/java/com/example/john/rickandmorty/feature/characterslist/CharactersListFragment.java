package com.example.john.rickandmorty.feature.characterslist;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.example.john.rickandmorty.R;
import com.example.john.rickandmorty.api.NetworkManager;
import com.example.john.rickandmorty.model.characters.Character;
import com.example.john.rickandmorty.model.characters.ResultsCharacters;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CharactersListFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_characters_list, null, false);

        final RecyclerView characterRecyclerView = rootView.findViewById(R.id.characters_recycler_view);
        characterRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        characterRecyclerView.setHasFixedSize(true);

        NetworkManager.getInstance().myWebService.getCharacters().enqueue(new Callback<ResultsCharacters>() {
            @Override
            public void onResponse(Call<ResultsCharacters> call, Response<ResultsCharacters> response) {
                if(response.body() != null){
                    List<Character> character = response.body().characterResults;
                    characterRecyclerView.setAdapter(new CharactersAdapter(character));
                } else {
                    Toast.makeText(getActivity(), "ERROR", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResultsCharacters> call, Throwable t) {
                t.printStackTrace();
            }
        });


        return rootView;
    }

}