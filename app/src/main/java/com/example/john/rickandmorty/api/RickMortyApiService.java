package com.example.john.rickandmorty.api;

import com.example.john.rickandmorty.model.characters.ResultsCharacters;
import com.example.john.rickandmorty.model.episodes.ResultsEpisodes;

import retrofit2.Call;
import retrofit2.http.GET;


public interface RickMortyApiService {

    @GET("character")
    public Call<ResultsCharacters>
    getCharacters();

    @GET("episode")
    public Call<ResultsEpisodes>
    getEpisodes();

}
