package com.example.john.rickandmorty.feature.characterslist;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.john.rickandmorty.R;
import com.example.john.rickandmorty.model.characters.Character;
import com.example.john.rickandmorty.feature.details.CharactersDetailsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class CharactersAdapter extends RecyclerView.Adapter<CharactersAdapter.CharactersViewHolder> {

    List<Character> charactersDataset = new ArrayList<>();

    @NonNull
    @Override
    public CharactersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_character, viewGroup, false);

        return new CharactersViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final CharactersViewHolder charactersViewHolder, int position) {

        final Character character = charactersDataset.get(position);

        charactersViewHolder.nameTextView.setText(charactersDataset.get(position).name);

        Picasso.get().load(character.image).into(charactersViewHolder.listImageView);

        charactersViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CharactersDetailsActivity.class);
                intent.putExtra("character", character);
                view.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return charactersDataset.size();
    }

    public CharactersAdapter(List<Character> characters){

        this.charactersDataset = characters;

    }


    public static class CharactersViewHolder extends RecyclerView.ViewHolder {

        TextView nameTextView;

        ImageView listImageView;

        public CharactersViewHolder (View view) {

            super(view);

            nameTextView = view.findViewById(R.id.name_text_view);

            listImageView = view.findViewById(R.id.list_image_view);

        }
    }

}
