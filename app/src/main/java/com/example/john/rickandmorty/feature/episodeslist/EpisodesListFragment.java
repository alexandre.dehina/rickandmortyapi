package com.example.john.rickandmorty.feature.episodeslist;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.example.john.rickandmorty.R;
import com.example.john.rickandmorty.api.NetworkManager;
import com.example.john.rickandmorty.api.RickMortyApiService;
import com.example.john.rickandmorty.model.episodes.Episode;
import com.example.john.rickandmorty.model.episodes.ResultsEpisodes;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class EpisodesListFragment extends Fragment {

    public static final String BASE_URL = "https://rickandmortyapi.com/api/";

    Retrofit retrofit;

    RickMortyApiService myWebService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_episodes_list, null, false);

        retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        myWebService = retrofit.create(RickMortyApiService.class);

        final RecyclerView episodeRecyclerView = rootView.findViewById(R.id.episodes_recycler_view);
        episodeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        NetworkManager.getInstance().myWebService.getEpisodes().enqueue(new Callback<ResultsEpisodes>() {
            @Override
            public void onResponse(Call<ResultsEpisodes> call, Response<ResultsEpisodes> response) {
                if(response.body() != null){
                    List<Episode> episode = response.body().episodeResults;
                    episodeRecyclerView.setAdapter(new EpisodesAdapter(episode));
                } else {
                    Toast.makeText(getActivity(), "ERROR", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResultsEpisodes> call, Throwable t) {
                t.printStackTrace();
            }
        });

        return rootView;
    }

}