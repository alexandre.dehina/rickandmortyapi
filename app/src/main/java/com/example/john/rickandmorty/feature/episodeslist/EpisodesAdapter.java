package com.example.john.rickandmorty.feature.episodeslist;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.john.rickandmorty.R;
import com.example.john.rickandmorty.feature.details.EpisodesDetailsActivity;
import com.example.john.rickandmorty.model.episodes.Episode;

import java.util.List;

public class EpisodesAdapter extends RecyclerView.Adapter<EpisodesAdapter.EpisodesViewHolder> {

    List<Episode> episodesDataSet;

    @NonNull
    @Override
    public EpisodesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_episode, viewGroup, false);

        return new EpisodesViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final EpisodesViewHolder episodesViewHolder, int position) {

        final Episode episode = episodesDataSet.get(position);

        String episodeName = episode.name;

        String episodeEpisode = episode.episode;

        episodesViewHolder.nameTextView.setText(episodeName);

        episodesViewHolder.episodeTextView.setText(episodeEpisode);

        episodesViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), EpisodesDetailsActivity.class);
                intent.putExtra("episode", episode);
                view.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return episodesDataSet.size();
    }

    public EpisodesAdapter(List<Episode> episode){

        this.episodesDataSet = episode;

    }

    public static class EpisodesViewHolder extends RecyclerView.ViewHolder {

        TextView nameTextView, episodeTextView;

        public EpisodesViewHolder (View view) {

            super(view);

            nameTextView = view.findViewById(R.id.name_text_view);

            episodeTextView = view.findViewById(R.id.episode_text_view);

        }
    }

}
