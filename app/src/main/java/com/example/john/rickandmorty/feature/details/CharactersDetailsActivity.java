package com.example.john.rickandmorty.feature.details;

import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.john.rickandmorty.R;
import com.example.john.rickandmorty.model.characters.Character;
import com.squareup.picasso.Picasso;


public class CharactersDetailsActivity extends AppCompatActivity {

    Character character;

    TextView nameTextView, statusTextView, speciesTextView, originTextView;

    ImageView photoImageView;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.character_details);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        setTitle("Character");

        character = getIntent().getParcelableExtra("character");

        nameTextView = findViewById(R.id.name_text_view);
        statusTextView = findViewById(R.id.status_text_view);
        speciesTextView = findViewById(R.id.species_text_view);
        originTextView = findViewById(R.id.origin_text_view);
        photoImageView = findViewById(R.id.photo_image_view);


        nameTextView.setText(character.name);
        statusTextView.setText(character.status);
        speciesTextView.setText(character.species);
        originTextView.setText(character.origin.name);

        Picasso.get().load(character.image)
                .into(photoImageView);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
