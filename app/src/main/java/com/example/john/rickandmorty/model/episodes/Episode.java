package com.example.john.rickandmorty.model.episodes;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;


public class Episode implements Parcelable {

    public Episode() {

    }

    @SerializedName("id")
    public int id;

    @SerializedName("episode")
    public String episode;

    @SerializedName("name")
    public String name;

    @SerializedName("air_date")
    public String airdate;

    @SerializedName("characters")
    public ArrayList<Object> characters;


    protected Episode(Parcel in) {
        id = in.readInt();
        episode = in.readString();
        name = in.readString();
        airdate = in.readString();
        characters = in.readArrayList(null);
    }

    public static final Creator<Episode> CREATOR = new Creator<Episode>() {
        @Override
        public Episode createFromParcel(Parcel in) {
            return new Episode(in);
        }

        @Override
        public Episode[] newArray(int size) {
            return new Episode[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(episode);
        parcel.writeString(name);
        parcel.writeString(airdate);
        parcel.writeList(characters);
    }





}
