package com.example.john.rickandmorty.feature.details;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import com.example.john.rickandmorty.R;
import com.example.john.rickandmorty.model.episodes.Episode;


public class EpisodesDetailsActivity extends AppCompatActivity {

    Episode episode;

    TextView episodeTextView, nameTextView, dateTextView, numberOfCharactersTextView;

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
        setContentView(R.layout.episode_details);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        setTitle("Episode");

        episode = getIntent().getParcelableExtra("episode");

        episodeTextView = findViewById(R.id.code_text_view);
        nameTextView = findViewById(R.id.name_text_view);
        dateTextView = findViewById(R.id.airdate_text_view);
        numberOfCharactersTextView = findViewById(R.id.number_text_view);

        episodeTextView.setText(episode.episode);
        nameTextView.setText(episode.name);
        dateTextView.setText(episode.airdate);
        numberOfCharactersTextView.setText(String.valueOf(episode.characters.size()));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
