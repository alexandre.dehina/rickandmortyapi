package com.example.john.rickandmorty.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class NetworkManager {

    static NetworkManager instance;

    public static final String BASE_URL = "https://rickandmortyapi.com/api/";

    Retrofit retrofit;

    public RickMortyApiService myWebService;

    public static NetworkManager getInstance() {

        if(instance == null){
            instance = new NetworkManager();
        }

        return instance;
    }

    public NetworkManager(){
        retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        myWebService = retrofit.create(RickMortyApiService.class);

    }
}
