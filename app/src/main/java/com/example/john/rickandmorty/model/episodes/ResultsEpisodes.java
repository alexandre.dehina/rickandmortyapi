package com.example.john.rickandmorty.model.episodes;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class ResultsEpisodes {

    public ResultsEpisodes() {

    }

    @SerializedName("results")
    public List<Episode> episodeResults;
}
