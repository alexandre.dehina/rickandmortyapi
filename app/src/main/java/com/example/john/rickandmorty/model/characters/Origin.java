package com.example.john.rickandmorty.model.characters;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Origin implements Parcelable {

    @SerializedName("name")
    public String name;

    @SerializedName("url")
    public String url;

    protected Origin(Parcel in){
        name = in.readString();
        url = in.readString();
    }

    public static final Creator<Origin> CREATOR = new Creator<Origin>() {
        @Override
        public Origin createFromParcel(Parcel in) {
            return new Origin(in);
        }

        @Override
        public Origin[] newArray(int size) {
            return new Origin[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(url);

    }
}
